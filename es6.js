//Use the let keyword in an example function
function add() {
  let a = 2,
    b = 3;
  console.log(a + b);
}

//Use the const keyword in an example function
function useConst() {
  const pi = 3.14;
  const r = 2;
  const circumference = 2 * pi * r;
  console.log(circumference);
}

//Create an arrow function that finds the square of a number
const square = (a) => {
  console.log(a * a);
};

//Create an arrow function that adds two numbers
const addition = (a, b) => {
  console.log(a + b);
};

//Create a multi-line string and then split the string into the corresponding lines and print the lines
let line = (str) => {
  str = `How to start learning web dev
Learn html
Learn css
Learn Javascript`;
  str.split("\n").forEach((element) => console.log(element));
};
// let str= `How to start learning web dev
// Learn html
// Learn css
// Learn Javascript`;

// const line = str.split('\n').forEach((element) => console.log(element))

/*Create a function that calculates the area of a circle. If the radius of the circle is not provided assume that the default radius is 5. 
Use the JavaScript default parameter feature to implement the function*/
function areaOfCircle(r = 5) {
  console.log("area of circle= " + 3.14 * r * r);
}

/*let person = {
    name: 'Harry Potter',
    location: 'London'
}
Create a string that prints the name and location of the person in below format:

1
"Harry Potter is located in London." */
let person = {
  name: "Harry Potter",
  location: "London",
};
let str1 = `${person.name} is located in ${person.location}`;
console.log(str1);

//Show an example where an array is destructured using destructuring assignment
let arr = [1, 2, 3, 4];
const [a, b] = arr;
console.log(b);

//Show an example where an object is destructured using destructuring assignment in the function body
function objDestruct() {
  const obj = { a: 1, b: 2, c: { d: 3 } };
  let { a, b } = obj;
  console.log(a, b);
}

//Show an example where a function argument which is an object is destructured inside the parantheses of the function
function destructobjj({ name, place, thing }) {
  console.log(`Name: ${name}`);
  console.log(`Place: ${place}`);
  console.log(`Thing: ${thing}`);
}

const use = {
  name: "Mayank",
  place: "New York",
  thing: "jet",
};

//Show an example where enhanced object literals is used.
function getLaptop(make, model, year) {
  return {
    make,
    model,
    year,
  };
}

console.log(getLaptop("Apple", "MacBook", "2015"));

//Create a function sum that takes any number of numbers as arguments and calculates the sum of the input numbers using the rest parameter syntax
function sum(...arg) {
  let total = 0;
  for (const i of arg) {
    total += i;
  }
  return total;
}
console.log(sum(1, 2, 3));

//Use the for..of loop to iterate through all values in an array
function forOf() {
  let arr1 = [2, 3, 4, 5, 6, 7];
  for (const number of arr1) {
    console.log(number);
  }
}

//Iterate through all keys of an object using Object.keys
//Iterate through all values of an object using Object.values
//Iterate through all the key / value pairs of an object using Object.entries
function iterateObj(){
    const obj={
        name : "Mayank",
        age : "24",
        place : "Ranchi",
    }
    console.log(Object.keys(obj));
    console.log(Object.values(obj));
    console.log(Object.entries(obj));
}

add();
useConst();
square(2);
addition(2, 3);
line();
areaOfCircle();
objDestruct();
destructobjj(use);
forOf();
iterateObj();
